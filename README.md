# Galaxy workflows tests

[![pipeline status](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/workflows-tests/badges/master/pipeline.svg)](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/workflows-tests/-/commits/master)

## Will use a set of workflows to test the health of usegalaxy.fr

However, even if the CI does not fail, you should check the status of the tests [here](https://gitlab.com/ifb-elixirfr/usegalaxy-fr/workflows-tests/-/pipelines).  
The first part of the CI will generate the planemo tests which will then be triggered.

---

## Local user

Workflows can be run by any user with his apikey.

### Create a local user

```bash
$ ssh $GALAXY_SERVER
$ virtualenv /tmp/venv_bioblend
$ source /tmp/venv_bioblend/bin/activate
$ pip install bioblend
(venv_bioblend) $ python
```

```python
from bioblend import galaxy
gi = galaxy.GalaxyInstance(url="localhost:8080", key="MASTER_API_KEY")
new_user = gi.users.create_local_user("usegalaxy_admin", "usegalaxy@france-bioinformatique.fr", "USEGALAXY_ADMIN_PASSWD")
gi.users.create_user_apikey(new_user["id"])
u'jtALCQgCZdBz2QxXMHNBqTXEwq37g7nr'
```

---

## Inputs file

The list of workflows to test is contained in `workflows.yml`.

Add workflow with the following pattern :

```yml
name of your test:
  {location (url or local file)}: "wf/url" (if url)
```

example for local worklow:

```yml
Galaxy_metagenomics-amplicon: local
```

Inputs files use to run workflows are stored at `/shared/ifbstor1/bank/galaxy_test-data/workflows`

example for online workflow

```yml
variation-reporting-sars-cov-2:
  url: "https://api.github.com/repos/iwc-workflows/sars-cov-2-variation-reporting/zipball"
```

The url must allow to directly download the workflow to test in zip format.  
For a workflow deposited on github, the url follows the following pattern

```url
https://api.github.com/repos/{user_name}/{repo_name}/zipball/
```

This one will download the last release.  
 If you want a specific release add `refs/tags/{tag_name}` at the end of the url

```url
https://api.github.com/repos/{user_name}/{repo_name}/zipball/refs/tags/{tag_name}
```

---

## Planemo is used to run workflow on Galaxy

### Install planemo

```bash
$ virtualenv .venv; . .venv/bin/activate
$ pip install "pip>=7" # Upgrade pip if needed.
$ pip install planemo
```

### Run planemo

```bash
$ . .venv/bin/activate
$ planemo test --galaxy_url "$galaxy_instance_url" --galaxy_user_key "$api_key" --no_shed_install --engine external_galaxy --history_name <history_name> <workflows.ga>
```
