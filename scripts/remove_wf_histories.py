from bioblend import galaxy
import argparse


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--history_names", help="Names of history to delete", required=True)
    parser.add_argument("--url", help="url of galaxy instance to copy", required=True)
    parser.add_argument("-k", "--key", help="admin apikey of instance to copy", required=True)
    args = parser.parse_args()
    return args


def main():

    args = get_args()

    # galaxy instance
    gi = galaxy.GalaxyInstance(url=args.url, key=args.key)

    # get histories list
    histories = galaxy.histories.HistoryClient(gi)
    histories_list = (histories.get_histories(history_id=None, name=None, deleted=False))

    # filter histories to remove
    # to_delete=['RNA_workflow_test','NGS_workflow_test','Galaxy_intro']
    to_delete = args.history_names.split(",")
    histories_list = [history for history in histories_list if history['name'] in to_delete]

    # remove histories
    for history in histories_list:
        histories.delete_history(history['id'], purge=True)
        print('history ' + history['name'] + " with id " + history['id'] + " has been removed and purged")


if __name__ == "__main__":
    # execute only if run as a script
    main()
