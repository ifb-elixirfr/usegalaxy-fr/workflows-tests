#!/usr/bin/env python
# encoding: UTF-8

import yaml
import argparse


def get_args():
    # useful if we need to quit gitlab or if we want to generate the pipeline manually
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--file", help="yaml file with wf name and url", required=True)
    args = parser.parse_args()
    return args


def main():
    # to write a pipeline from a list of workflows to test
    args = get_args()
    infile = args.file
    wfList = []
    print(".prod:")

    print("  tags:")
    print("    - docker")
    print("stages:")
    print("  - test")
    print("\n")

    with open(infile, 'r') as f:
        parsed = yaml.full_load(f)
        for name, path in parsed.items():
            wfList.append(name)  # for the cleanup stage
            print("{0}:".format(name))
            print("  stage: test")
            print("  image: registry.gitlab.com/ifb-elixirfr/usegalaxy-fr/workflows-tests")
            
            print("  script:")
            print("    - pip3 install virtualenv")
            print("    - virtualenv .venv; . .venv/bin/activate")
            print("    - pip3 install planemo")
            if 'url' in path:  # define workflow path
                print("    - wget {0} -P {1}".format(path['url'], name))
                print("    - unzip {0}/* -d $CI_PROJECT_DIR/{0}".format(name))
                wf = "$CI_PROJECT_DIR/"+name+"/*/*.ga"
            elif 'local' in path:
                print("    - unzip {0}.zip -d /".format(name))
                wf = "$CI_PROJECT_DIR/workflows/"+name+"/*.ga"
            elif 'web' in path:
                wf = "$CI_PROJECT_DIR/workflows/"+name+"/*.ga"
            print(
                "    - planemo test --galaxy_url \"$galaxy_instance_url\" --galaxy_user_key \"$api_key\" --no_shed_install --engine external_galaxy {1} --history_name {0} || FAILED=true".format(name, wf))  # "planemo test or FAILED=true" in order to continue the pipeline
            # to know if planemo test has failed
            print("    - if [ $FAILED ] ; then exit 1; fi ")
            print("  needs:")
            print("    - pipeline: $PARENT_PIPELINE_ID")
            print("      job: prepare-data")
            print("  artifacts:")
            print("    paths:")
            print("      - tool_test_output.html")
            print("    expire_in: 1 week")
            print("    when: on_failure")
            print("  retry: 2")
            print("\n")

    print("cleanup_histories:")
    print("  stage: .post")
    print("  image: registry.gitlab.com/ifb-elixirfr/usegalaxy-fr/workflows-tests")
    print("  extends:")
    print("    - .prod")
    print("  script:")
    print("    - pip3 install virtualenv")
    print("    - virtualenv .venv; . .venv/bin/activate")
    print("    - pip3 install planemo")
    print(
        "    - python scripts/remove_wf_histories.py --url \"$galaxy_instance_url\" --key \"$api_key\" --history_names {0}".format(','.join(wfList)))
    print("  when: always")


if __name__ == "__main__":
    # execute only if run as a script
    main()
