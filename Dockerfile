FROM ubuntu:20.04

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive


ENV PYTHON_VERSION='3.9'

# prepare
RUN apt update && apt upgrade -y && \
    apt install -y \
    apt-utils \
    apt-transport-https\
    ca-certificates\
    curl\
    gnupg-agent\
    software-properties-common\
    unzip \
    wget \
    python3-pip \
    python3.8-venv
